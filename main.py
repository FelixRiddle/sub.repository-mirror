import os
import json
import sys

from src.repository_services import RepositoryServices

'''Loads data from ./data/settings.json

'''
def get_settings():
    try:
        settings_path = os.getcwd() + "/data/settings.json"
        with open(settings_path, "r") as f:
            return json.load(f)
    except Exception as ex:
        return None

'''Starts the program

Uses data from ./data/settings.json.
Note that it needs two variables(the access
token and the username) for it to work.
{
    "githubAccessToken": "theToken",
    "username": "theUsername",
}
'''
def start():
    settings = get_settings()
    
    repo_services = RepositoryServices(settings)
    
    repo_services.fetch_github_rep_data()
    
    return repo_services

try:
    # My own submodules
    sys.path.append(
        "/home/fede/perseverancia/python_submodules/sub.check-submodules/")
    from check_submodule import is_submodule
    
    # Check if it's a submodule
    if not is_submodule("sub.repository-mirror"):
        start()
except Exception as ex:
    pass

'''Fetch repository list from the given user

This function needs at least two vars:
{
    "githubAccessToken": "theToken",
    "username": "theUsername",
}
'''
def fetch_repos(settings:dict):
    return RepositoryServices(settings)