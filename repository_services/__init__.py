import json
import os

from . import github_api
from .github_api import GithubApi

class RepositoryServices:
    gh_api = GithubApi()

    # The directory to save the data and the file name
    data_folder_name:str = f".{os.path.sep}data{os.path.sep}github{os.path.sep}"
    file_name:str = "repository_list.json"
    github_repo_list_path:str = f"{data_folder_name}{file_name}"
    
    settings = None
    updated:bool = False
    
    # Data stored locally
    sep:str = os.path.sep
    data_path:str = f".{sep}data"
    github_data_path:str = f".{sep}data{sep}github"
    github_file_path:str = f".{sep}data{sep}github{sep}repository_list.json"
    
    # Fetched data
    repository_list:list = []
    
    sql_instance = None
    
    def __init__(self, settings=None, token:str="", username:str="", debug:bool=False):
        self.settings = settings
        self.gh_api.debug = debug
        
        # Validate data
        if self.validate_data():
            # Set data
            self.gh_api.token = settings["githubAccessToken"]
            self.gh_api.username = settings["username"]
        
        # Replace the settings for these if they are given
        if len(token) > 0:
            self.gh_api.token = token
        
        if len(username) > 0:
            self.gh_api.username = username
    
    '''Print rate limits'''
    def print_rate_limits(self):
        # Print rate limits
        return self.gh_api.print_rate_limits()
    
    '''Get repository list'''
    def get_repository_list(self) -> list:
        return self.repository_list
    
    '''Get a list of repository services available'''
    def get_repository_services_available(self) -> list:
        return [self.gh_api]
    
    '''Get rate limits'''
    def get_rate_limits(self):
        return self.gh_api.get_rate_limits()
    
    '''For testing'''
    def ping(self):
        print("Pong - repository_services")
    
    '''Validate the given data to this object
    
    Currently validates username and githubAccessToken'''
    def validate_data(self):
        # If there are no env variables, return
        settings = self.settings
        if(not self.settings) or ((not "username" in settings) and (not "githubAccessToken" in settings)):
            return False
        return True
    
    '''Fetch repository by name'''
    def fetch_user_repository_json(self, repository_name: str = None):
        return self.gh_api.fetch_user_repository_json(repository_name=repository_name)
    
    '''Fetch data from github repositories
    
    Requires authentication'''
    def fetch_github_rep_data(self,
            options:dict={ "query_params": { "per_page": 100, }, "sleep": 10, },
            per_page:int=None,
            sleep:int=None,
            max_repos:int=1):
        # Get every repository
        self.repository_list = self.gh_api.fetch_user_repositories_json(
            options=options,
            per_page=per_page,
            sleep=sleep,
            max_repos=max_repos)
        
        # Set the updated flag to True
        self.updated = True
        
        return self.repository_list
