<h1>Quick guide and other stuff</h1>
<p>
    <strong>
        Note: It's recommended to read doc/index.html instead.
    </strong>
    To read it just double click doc/index.html and it will be</br>
    open on the browser</br>
</p>
<h2><strong>Submodules dependencies and how to setup them</strong></h2>
<div style="font-size: smaller;">
    <h4>
        Method 1:
    </h4>
    </br>
    Create a folder called modules on the root folder, e.g:</br>
    \ project_name</br>
    --\ modules \ &lt;--- Create this folder</br>
    --\ main.py</br>
    --\ README.md</br>
    --\ ...</br>
    </br>
    Then add the following submodules using the 'git submodule add' under</br>
    the new folder:</br>
    From <a href="https://github.com/Perseverancia-company">Perseverancia-company</a>:</br>
    <ul>
        <li>chunk-storing</li>
    </ul>
    </br>
    The end result should look like this:</br>
    \ project_name</br>
    --\ modules</br>
    --|-\ chunk-storing</br>
    --\ main.py</br>
    --\ README.md</br>
    --\ ...</br>
    </br>
    <h4>Method 2</h4></br>
    On home directory(~) create folders perseverancia/python_submodules/</br>
    and there, clone the repository from <a src="https://github.com/Perseverancia-company">Perseverancia-company</a></br>
    called "sub.check-submodules"</br>
    The end result should look like this:</br>
    ~\ perseverancia</br>
    --\ python_submodules</br>
    --|-\ sub.check-submodules &lt;--- The cloned repository</br>
    --|-\ sub.chunk-storing &lt;--- "" "" ""</br>
    --|-\ sub.repository-mirror &lt;--- This very repository</br>
</div>
<h2><strong>Useful commands</strong></h2>

```bash
git config alias.supdate 'submodule update --remote --init --recursive --merge'
```

<h2><strong>Access</h2></strong>
In ./data/settings.json add the following variables:</br>

```json
{
    "githubAccessToken": "YourAccessToken",
    "username": "YourUsername"
}
```
